package ru.god.supercalc;

import java.util.Scanner;

/**
 * Класс предназначен для демонстрации работы класса с методами,
 * в которых реализованы математические операции
 *
 * @author Горбачева, 16ИТ18к
 */

public class DemoCalc {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Добрый день! Вас приветствует программа СУПЕР_КАЛК\nВ ее функционал входит:\n1. сложение (+);\n2. вычитание (-);\n3. умножение (*);\n4. деление вещественное (/)\n5. деление целочисленное (/);\n6. выделение остатка (%);\n7. возведение в степень (^).\nВведите выражение, значение которого необходимо вычислить");
        String expression = scanner.nextLine();
        boolean doubleOrInt = expression.contains(".");
        String[] array = expression.split(" ");
        double a = Double.parseDouble(array[0]);
        double b = Double.parseDouble(array[2]);
        int c;
        int d;
        switch (array[1]) {
            case "+": {
                System.out.println(expression + " = " + SuperCalc.amount(a, b));
                break;
            }
            case "-": {
                System.out.println(expression + " = " + SuperCalc.residual(a, b));
                break;
            }
            case "*": {
                System.out.println(expression + " = " + SuperCalc.multiplication(a, b));
                break;
            }
            case "%": {
                c = Integer.parseInt(array[0]);
                d = Integer.parseInt(array[2]);
                while (d == 0) {
                    System.out.println("Делитель введен некорректно. Попробуйте ввести другое значение");
                    d = scanner.nextInt();
                }
                System.out.println(c + " % " + d + " = " + SuperCalc.rest(c, d));
                break;
            }
            case "^": {
                c = Integer.parseInt(array[2]);
                System.out.println(a + " ^ " + c + " = " + SuperCalc.pow(a, c));
                break;
            }
            case "/": {
                if (doubleOrInt) {
                    while (b == 0) {
                        System.out.println("Делитель введен некорректно. Попробуйте ввести другое значение");
                        b = scanner.nextDouble();
                    }
                    System.out.println(a + " / " + b + " = " + SuperCalc.division(a, b));
                } else {
                    c = Integer.parseInt(array[0]);
                    d = Integer.parseInt(array[2]);
                    while (d == 0) {
                        System.out.println("Делитель введен некорректно. Попробуйте ввести другое значение");
                        d = scanner.nextInt();
                    }
                    System.out.println(c + " / " + d + " = " + SuperCalc.division(c, d));
                }
                break;
            }
        }
    }
}