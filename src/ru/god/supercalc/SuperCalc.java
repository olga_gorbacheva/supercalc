package ru.god.supercalc;

/**
 * <h1>Класс, включающий в себя методы для выполнения примитивных математических вычислений:</h1>
 * <ol>
 * <li>сложение;</li>
 * <li>вычитание;</li>
 * <li>умножение;</li>
 * <li>деление вещественное;</li>
 * <li>деление целочисленное;</li>
 * <li>выделение остатка;</li>
 * <li>возведение в степень;</li>
 * </ol>
 * @author Горбачева, 16ИТ18к
 */
public class SuperCalc {
    /**
     * Метод возвращает значение суммы двух чисел
     *
     * @param a - первое слагаемое
     * @param b - второе слагаемое
     * @return сумма a и b
     */
    public static double amount(double a, double b) {
        return a + b;
    }

    /**
     * Метод возвращает значение разности двух чисел
     *
     * @param a - уменьшаемое
     * @param b - вычитаемое
     * @return разность a и b
     */
    public static double residual(double a, double b) {
        return a - b;
    }

    /**
     * Метод возвращает значение произведения двух чисел
     *
     * @param a - первый множитель
     * @param b - второй множитель
     * @return произведение a и b
     */
    public static double multiplication(double a, double b) {
        return a * b;
    }

    /**
     * Метод возвращает значение частного вещественного типа двух чисел
     *
     * @param a - делимое
     * @param b - делитель
     * @return частное a и b вещественного типа
     */
    public static double division(double a, double b) {
        return a / b;
    }

    /**
     * Метод возвращает значение частного целочисленного типа двух чисел
     *
     * @param a - делимое
     * @param b - делитель
     * @return частное a и b целочисленного типа
     */
    public static int division(int a, int b) {
        return a / b;
    }

    /**
     * Метод возвращает остаток от деления одного аргумента на второй
     *
     * @param a - делимое
     * @param b - делитель
     * @return остаток от деления a на b
     */
    public static int rest(int a, int b) {
        return a % b;
    }

    /**
     * Метод возвращает результат возведения числа в степень
     *
     * @param a - основание степени
     * @param b - показатель степени
     * @return значение a в степени b
     */
    public static double pow(double a, int b) {
        double result = 1;
        switch (b) {
            case -1: {
                return 1 / a;
            }
            case 0: {
                return result;
            }
            case 1: {
                return a;
            }
            default: {
                for (int i = 0; i < Math.abs(b); i++) {
                    result *= a;
                }
                if (b < -1) {
                    return 1 / result;
                } else {
                    return result;
                }
            }
        }
    }
}